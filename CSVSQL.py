"""
Authors: MONT Louis
         MONTOUCHET Teddy
"""
import csv
import os
import sqlite3
import datetime
import logging


def verifCSVFile(file):
    """
    Verifie si le chemin existe et si le fichier est tu type demandé.

    Parameters:
        file: Le fichier que l'on vérifie.

    Returns:
        bool: True si la vérification est ok, false sinon.
    """
    if(os.path.exists(file)):
        if(os.path.isfile(file)):
            return True
        else:
            logging.warning("Path %s do not exists" % file)
    else:
        logging.warning("File %s is not of expected type" % file)
    return False


def getCSV(file, delimiter):
    """
    Décrypte le CSV.

    Parameters:
        file: le fichier à lire.
        delimiter:le délimiteur entre les données.

    Returns:
        Retourne la liste de listes composée de tout les éléments du CSV.
    """
    thisdelimiter = delimiter
    if(verifCSVFile(file)):
        with open(file) as csvfile:
            reader = csv.reader(csvfile, delimiter=thisdelimiter)
            logging.info("File opened")
            dataRows = []
            for row in reader:
                dataRows.append(row)
            return dataRows


def upsertBDD(linesCreated, linesModified, tablename, row, attributes, primary, cursor):
    """
    Modifie ou crée la ligne.
    Incrémente linesCreated ou linesModified suivant l'action effectuée.
    Parameters:
        linesCreated: le nombre de lignes déjà crées.
        linesModified: le nombre de lignes déjà modifiées.
        tablename: le nom de la table que l'on crée.
        row: la ligne que l'on cherche à entrer dans la base de donnée, par modification ou création.
        attributes: la ligne contenant tout les attributs dans le CSV.
        primary: le nom de la colonne dans laquelle est stockée les clés primaires de la table.
        cursor: le curseur qui pointe sur la base de donnée que l'on lit.

    Returns:
        linesCreated: incrémentée si une ligne sera créée.
        linesModified: incrémentée si une ligne sera créée.
    """
    valPrimary = row[attributes.index(primary)]
    command = str.format('''SELECT * FROM %s WHERE %s=?;''' %
                         (tablename, primary))
    cursor.execute(command, (valPrimary,))
    lastrequest = cursor.fetchone()
    if(lastrequest == None):
        logging.debug("Row %s not found, creating..." % valPrimary)
        insertBDD(tablename, row, attributes, cursor)
        linesCreated += 1
    else:        
        linesModified += updateBDD(tablename, row,
                                   lastrequest, attributes, primary, cursor)
    return (linesCreated, linesModified)


def updateBDD(tablename, row, BDDline, attributes, primary, cursor):
    """
    Update dans la ligne des données demandant l'update

    Parameters:   
        tablename: le nom de la table que l'on crée.           
        row: la ligne que l'on cherche à update dans la base de données.
        BDDline: la représentation de la ligne dans la base de données.
        attributes: la ligne contenant tout les attributs dans le CSV.
        primary: le nom de la colonne dans laquelle est stockée les clés primaires de la table.
        cursor: le curseur qui pointe sur la base de donnée que l'on lit.        

    """
    toChange = []
    for i in range(0, len(attributes)):
        if(row[i] != BDDline[i]):
            toChange.append(row[i])
    for changeRow in toChange:
        args = (changeRow, row[attributes.index(primary)])
        command = str.format('''UPDATE %s SET %s=? WHERE %s=?;''' % (
            tablename, attributes[row.index(changeRow)], primary))
        cursor.executemany(command, (args,))
    if(toChange == []):
        return 0
    else:
        logging.debug("Row %s found, modifying..." % row[attributes.index(primary)])
        return 1


def insertBDD(tablename, row, attributes, cursor):
    """
    Insertion dans la base de données des données dans chaque ligne, créée si non-existante.

    Parameters:         
        tablename: le nom de la table que l'on crée.
        row: la ligne que l'on cherche à update dans la base de données.
        attributes: la ligne contenant tout les attributs dans le CSV.
        cursor: le curseur qui pointe sur la base de donnée que l'on lit.

    """
    tupleIndexes = "?,"*len(attributes)
    tupleIndexes = tupleIndexes[:-1]
    command = str.format('''INSERT INTO %s VALUES (%s);''' %
                         (tablename, tupleIndexes))
    cursor.executemany(command, (row,))


def modifyRows(dataRows, tablename, primary, sqlFile):
    """
    Modifie les lignes de la base de données pour rajouter ou modifier celle correspondante.

    Parameters:
        dataRows: La liste contenant toutes les lignes du .csv.
        tablename: le nom de la table que l'on crée.
        primary: le nom de la colonne dans laquelle est stockée les clés primaires de la table.
        sqlFile: Le fichier SQL dans lequel on lit/écrit.

    """
    connection = sqlite3.connect(sqlFile)
    logging.debug("Connected to file")
    cursor = connection.cursor()
    attributes = dataRows.pop(0)
    linesCreated = 0
    linesModified = 0
    for row in dataRows:
        (linesCreated, linesModified) = upsertBDD(
            linesCreated, linesModified, tablename, row, attributes, primary, cursor)
    connection.commit()
    logging.debug("Commands committed, %s lines created, %s lines modified" % (
        linesCreated, linesModified))
    cursor.close()
    connection.close()


def createBDD(tablename, cursor):
    """
    Crée la base de données

    Parameters
        tablename: le nom de la table que l'on crée.
        cursor: le curseur qui pointe sur la base de donnée que l'on lit.    
    """
    logging.debug("No table existing, creating...")
    command = str.format("""CREATE TABLE %s (adresse_titulaire TEXT,nom TEXT,prenom TEXT,immatriculation TEXT,date_immatriculation TEXT,vin TEXT,marque TEXT,denomination_commerciale TEXT,couleur TEXT,carrosserie TEXT,categorie TEXT,cylindree TEXT,energie TEXT,places TEXT,poids TEXT,puissance TEXT,type TEXT,variante TEXT,version TEXT);""" % tablename)
    cursor.execute(command)
    logging.debug("Table created")


def verifBDDAndSIV(attributeRow, tablename, sqlFile):
    """
    Vérifie si la BDD et/ou la table sont créées, les crée sinon

    Parameters:
        attributeRow: la ligne contenant tout les attributs dans le CSV.
        tablename: le nom de la table que l'on crée.
        sqlFile: le fichier dans lequel est enregistré la base de données.

    """
    if(not os.path.exists(sqlFile)):
        logging.debug("File does not exist, creating...")
    connection = sqlite3.connect(sqlFile)
    logging.debug("Connected to file")
    cursor = connection.cursor()
    cursor.execute("SELECT name FROM sqlite_master WHERE type='table';")
    if(cursor.fetchall() == []):
        createBDD(tablename, cursor)
    connection.commit()
    logging.debug("Commands committed")
    cursor.close()
    connection.close()


def main(file, delimiter, tablename, primary, sqlFile):
    """
    Main

    Parameters:
        file: le fichier à lire.
        delimiter:le délimiteur entre les données.
        primary: le nom de la colonne dans laquelle est stockée les clés primaires de la table.
        sqlFile: le fichier dans lequel est enregistré la base de données.        

    """
    logging.basicConfig(filename='CSVSQL.log',
                        level=logging.DEBUG, format='%(asctime)s: %(message)s')
    dataRows = getCSV(file, delimiter)
    logging.info("CSV get in list of lists")
    verifBDDAndSIV(dataRows[0], tablename, sqlFile)
    logging.info("DB and Table created if they were not existing")
    modifyRows(dataRows, tablename, primary, sqlFile)
    logging.info("DB Updated")


if __name__ == "__main__":
    main("newauto.csv", ';', 'SIV', 'vin', 'BDDCSV.sqlite3')
