import unittest
import csv
import sqlite3
import os
import datetime
from CSVSQL import *

class TestCSVSQL(unittest.TestCase):
    def setUp(self):
        self.tablename='SIV'
        logging.basicConfig(filename='test.log',level=logging.DEBUG, format='%(asctime)s: %(message)s')    
        self.connection=sqlite3.connect('test.sqlite3')        
        cursor=self.connection.cursor()
        cursor.execute("CREATE TABLE SIV (ID TEXT,NAME TEXT,DUMMY TEXT);")
        cursor.close()
            
    def tearDown(self):
        logging.shutdown()                    
        self.connection.close()
        os.remove('test.sqlite3')
        try:
            os.remove('test.log')
        except:
            pass
        


    def test_verifCSVFile(self):
        self.assertEqual(True,verifCSVFile('test.sqlite3'))


    def test_upsertBDD(self):
        #also tests update and insert since they are in the upsert function
        primary='id'
        cursor=self.connection.cursor()        
        attribute=['id','name','dummy']
        row=['1','bonjour','obiwan']
        cursor.execute('SELECT*FROM SIV;')
        self.assertEqual(None,cursor.fetchone())
        self.assertEqual((1,0),upsertBDD(0,0,self.tablename,row,attribute,primary,cursor))
        cursor.execute('SELECT*FROM SIV;')
        self.assertEqual(len(attribute),len(cursor.fetchone()))
        row=['1','hello','there']        
        cursor.execute('SELECT*FROM SIV;')
        self.assertEqual(len(attribute),len(cursor.fetchone()))        
        self.assertEqual((0,1),upsertBDD(0,0,self.tablename,row,attribute,primary,cursor))
        cursor.execute('SELECT*FROM SIV;')
        self.assertEqual(3,len(cursor.fetchone()))        
        cursor.close()